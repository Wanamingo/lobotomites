[
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 0,
      "Name" : "Eyebrow Main"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 1,
      "Name" : "Eyebrow In"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 2,
      "Name" : "Eyebrow Outer"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 3,
      "Name" : "Cheek Bones"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 4,
      "Name" : "Cheeks"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 5,
      "Name" : "Ears Main"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 6,
      "Name" : "Ears Top"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 7,
      "Name" : "Ears Mid"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 8,
      "Name" : "Ears Bottom"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 9,
      "Name" : "Nose Main"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 10,
      "Name" : "Nostrils"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646266,
      "Name" : "Jaw Outer"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646267,
      "Name" : "Jaw Mid"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646268,
      "Name" : "Temple"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646269,
      "Name" : "Chin"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646270,
      "Name" : "Neck Fat"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646271,
      "Name" : "Eyes"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646272,
      "Name" : "Cheek Bones Back"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646273,
      "Name" : "Nose Bridge"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646274,
      "Name" : "Eyebrows Mid"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646275,
      "Name" : "Nose Tip"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646276,
      "Name" : "Mouth Main"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646277,
      "Name" : "Mouth Corners"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646278,
      "Name" : "Mouth Top"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 921646279,
      "Name" : "Mouth Bot"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 25,
      "Name" : "Forehead"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 26,
      "Name" : "Lower Face"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 27,
      "Name" : "Side Nose Fat"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 60,
      "Name" : "Eyelids - Top"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 61,
      "Name" : "Eyelids - Bottom"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 62,
      "Name" : "Jowls -  Lower"
   },
   {
      "BonesA" : null,
      "Defaults" : {
         "Position" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Rotation" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         },
         "Scale" : {
            "x" : 0,
            "y" : 0,
            "z" : 0
         }
      },
      "ID" : 63,
      "Name" : "Nose - Ridge"
   }
]
