Scriptname AAAAWanaBombDoorTest00:NewScript extends ObjectReference


Group Optional_Properties
	ObjectReference Property TriggeredByReference Auto Const
	ObjectReference Property PlatformReference Auto Const
	explosion property FatManExplosionMIRVchild Auto Const
EndGroup
Objectreference MahExplody00


Event OnTriggerEnter(ObjectReference akActionRef)
	Debug.trace(akActionRef + "Someone just entered!")
	If (akActionRef == TriggeredByReference as ObjectReference)
;	if (DefaultScriptFunctions.CheckForMatches(RefToCheck = akActionRef)
		Debug.trace(akActionRef + "Bomb just entered!")
		MahExplody00 = self.placeatme(FatManExplosionMIRVchild)
		utility.wait(0.5)
		PlatformReference.disable()
		PlatformReference.delete()
		TriggeredByReference.disable()
		TriggeredByReference.delete()
		utility.wait(0.5)
		MahExplody00.DeleteWhenAble()
		self.disable()
		self.delete()
	endif

EndEvent
