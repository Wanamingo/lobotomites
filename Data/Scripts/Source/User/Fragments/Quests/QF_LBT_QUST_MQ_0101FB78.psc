;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Quests:QF_LBT_QUST_MQ_0101FB78 Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0000_Item_00
Function Fragment_Stage_0000_Item_00()
;BEGIN CODE
pLBT_GLOB_BRAINAlertStatus.SetValue(1)
AdminDoors1Ref.GetRef().Lock();
AdminDoors2Ref.GetRef().Lock();
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0010_Item_00
Function Fragment_Stage_0010_Item_00()
;BEGIN CODE
SetObjectiveDisplayed(100)
pLBT_GLOB_BRAINAlertStatus.SetValue(1)
AdminDoors1Ref.GetRef().Lock();
AdminDoors2Ref.GetRef().Lock();
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0015_Item_00
Function Fragment_Stage_0015_Item_00()
;BEGIN CODE
SetObjectiveDisplayed(100)
SetObjectiveDisplayed(110)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0020_Item_00
Function Fragment_Stage_0020_Item_00()
;BEGIN CODE
SetObjectiveDisplayed(110)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0025_Item_00
Function Fragment_Stage_0025_Item_00()
;BEGIN CODE
SetObjectiveDisplayed(120)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0027_Item_00
Function Fragment_Stage_0027_Item_00()
;BEGIN CODE
SetObjectiveCompleted(120)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0030_Item_00
Function Fragment_Stage_0030_Item_00()
;BEGIN CODE
SetObjectiveCompleted(110)
SetObjectiveDisplayed(130)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0033_Item_00
Function Fragment_Stage_0033_Item_00()
;BEGIN CODE
SetObjectiveCompleted(130)
SetObjectiveDisplayed(135)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0034_Item_00
Function Fragment_Stage_0034_Item_00()
;BEGIN CODE
SetObjectiveCompleted(135)
SetObjectiveDisplayed(150)
SetObjectiveDisplayed(160)
SetObjectiveDisplayed(170)
SetObjectiveDisplayed(180)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0045_Item_00
Function Fragment_Stage_0045_Item_00()
;BEGIN CODE
Debug.Trace("Player got a LBT_QUST_MQ item.")
if (GetStageDone(50) && GetStageDone(52) && GetStageDone(54)&& GetStageDone(56))
    SetStage(60)
    Debug.Trace("Player got a all the required LBT_QUST_MQ items")
EndIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0050_Item_00
Function Fragment_Stage_0050_Item_00()
;BEGIN CODE
SetObjectiveCompleted(150)
Debug.Trace("Player got a LBT_QUST_MQ item.")
if (IsObjectiveCompleted(150) && IsObjectiveCompleted(160) && IsObjectiveCompleted(170)&& IsObjectiveCompleted(180))
    SetStage(58)
    Debug.Trace("Player got a all the required LBT_QUST_MQ items")
EndIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0052_Item_00
Function Fragment_Stage_0052_Item_00()
;BEGIN CODE
LBT_Alias_UtilityLightControlMarker.GetRef().Disable()
SetObjectiveCompleted(160)
Debug.Trace("Player got a LBT_QUST_MQ item.")
if (IsObjectiveCompleted(150) && IsObjectiveCompleted(160) && IsObjectiveCompleted(170)&& IsObjectiveCompleted(180))
    SetStage(58)
    Debug.Trace("Player got a all the required LBT_QUST_MQ items")
EndIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0054_Item_00
Function Fragment_Stage_0054_Item_00()
;BEGIN CODE
SetObjectiveCompleted(170)
Debug.Trace("Player got a LBT_QUST_MQ item.")
if (IsObjectiveCompleted(150) && IsObjectiveCompleted(160) && IsObjectiveCompleted(170)&& IsObjectiveCompleted(180))
    SetStage(58)
    Debug.Trace("Player got a all the required LBT_QUST_MQ items")
EndIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0056_Item_00
Function Fragment_Stage_0056_Item_00()
;BEGIN CODE
SetObjectiveCompleted(180)
Debug.Trace("Player got a LBT_QUST_MQ item.")
if (IsObjectiveCompleted(150) && IsObjectiveCompleted(160) && IsObjectiveCompleted(170)&& IsObjectiveCompleted(180))
    SetStage(58)
    Debug.Trace("Player got a all the required LBT_QUST_MQ items")
EndIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0058_Item_00
Function Fragment_Stage_0058_Item_00()
;BEGIN CODE
SetObjectiveDisplayed(182)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0060_Item_00
Function Fragment_Stage_0060_Item_00()
;BEGIN CODE
SetObjectiveCompleted(182)
SetObjectiveDisplayed(184)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0062_Item_00
Function Fragment_Stage_0062_Item_00()
;BEGIN CODE
SetObjectiveCompleted(184)
SetObjectiveDisplayed(190)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0064_Item_00
Function Fragment_Stage_0064_Item_00()
;BEGIN CODE
SetObjectiveCompleted(190)
pLBT_GLOB_BRAINAlertStatus.SetValue(0)
; get the refsssssss and unlock the doors
AdminDoors1Ref.GetRef().Lock(false, true);
AdminDoors2Ref.GetRef().Lock(false, true);
; Open the god damn doors
AdminDoors1Ref.GetRef().Activate(Game.GetPlayer());
AdminDoors2Ref.GetRef().Activate(Game.GetPlayer());
SetObjectiveDisplayed(200)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0066_Item_00
Function Fragment_Stage_0066_Item_00()
;BEGIN CODE
SetObjectiveCompleted(190)
pLBT_GLOB_BRAINAlertStatus.SetValue(0)
; get the refsssssss and unlock the doors
AdminDoors1Ref.GetRef().Lock(false, true);
AdminDoors2Ref.GetRef().Lock(false, true);
; Open the god damn doors
AdminDoors1Ref.GetRef().Activate(Game.GetPlayer());
AdminDoors2Ref.GetRef().Activate(Game.GetPlayer());
SetObjectiveDisplayed(200)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0075_Item_00
Function Fragment_Stage_0075_Item_00()
;BEGIN CODE
SetObjectiveCompleted(210)
SetObjectiveDisplayed(220)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0077_Item_00
Function Fragment_Stage_0077_Item_00()
;BEGIN CODE
SetObjectiveCompleted(220)
SetObjectiveDisplayed(240)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0080_Item_00
Function Fragment_Stage_0080_Item_00()
;BEGIN CODE
SetObjectiveCompleted(240)
SetObjectiveDisplayed(250)
SetObjectiveDisplayed(260)
SetObjectiveDisplayed(270)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0082_Item_00
Function Fragment_Stage_0082_Item_00()
;BEGIN CODE
SetObjectiveCompleted(250)
SetObjectiveFailed(260)
SetObjectiveFailed(270)
SetObjectiveDisplayed(280)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0084_Item_00
Function Fragment_Stage_0084_Item_00()
;BEGIN CODE
SetObjectiveFailed(250)
SetObjectiveCompleted(260)
SetObjectiveFailed(270)
SetObjectiveDisplayed(280)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0086_Item_00
Function Fragment_Stage_0086_Item_00()
;BEGIN CODE
SetObjectiveFailed(250)
SetObjectiveFailed(260)
SetObjectiveCompleted(270)
SetObjectiveDisplayed(280)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0090_Item_00
Function Fragment_Stage_0090_Item_00()
;BEGIN CODE
pLBT_CaveDoorExplosionMarker.placeatme(pBreakableRockWallGate01explosion)
pFXExplosionEyebotStonebreakingPulseFinal.Play(pLBT_CaveDoorExplosionMarker)
Debug.Trace("Enabling outside entrance and removing rock")
pLBT_CaveEntranceOutside.GetRef().Disable()
Debug.MessageBox("The satellite relay signal has been hijacked. Schematics have been downloaded in the Pipboy")
SetStage(100)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0090_Item_01
Function Fragment_Stage_0090_Item_01()
;BEGIN CODE
pLBT_CaveDoorExplosionMarker.placeatme(pBreakableRockWallGate01explosion)
pFXExplosionEyebotStonebreakingPulseFinal.Play(pLBT_CaveDoorExplosionMarker)
Debug.Trace("Enabling outside entrance and removing rock")
pLBT_CaveEntranceOutside.GetRef().Disable()
Debug.MessageBox("The satellite relay signal has been disabled. Schematics have been downloaded in the Pipboy")
SetStage(100)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0090_Item_02
Function Fragment_Stage_0090_Item_02()
;BEGIN CODE
pLBT_CaveDoorExplosionMarker.placeatme(pBreakableRockWallGate01explosion)
pFXExplosionEyebotStonebreakingPulseFinal.Play(pLBT_CaveDoorExplosionMarker)
Debug.Trace("Enabling outside entrance and removing rock")
pLBT_CaveEntranceOutside.GetRef().Disable()
Debug.MessageBox("The satellite relay signal has been boosted. Schematics have been downloaded in the Pipboy")
SetStage(100)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0100_Item_00
Function Fragment_Stage_0100_Item_00()
;BEGIN CODE
CompleteAllObjectives()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property pLBT_GLOB_BRAINAlertStatus Auto Const

Alias Property AdminDoors1 Auto Const

Alias Property AdminDoors2 Auto Const

ReferenceAlias Property AdminDoors1Ref Auto Const

ReferenceAlias Property AdminDoors2Ref Auto Const

ReferenceAlias Property LBT_Alias_UtilityLightControlMarker Auto Const

Explosion Property pBreakableRockWallGate01explosion Auto Const

Sound Property pFXExplosionEyebotStonebreakingPulseFinal Auto Const

ObjectReference Property pLBT_CaveDoorExplosionMarker Auto Const

ReferenceAlias Property pLBT_CaveEntranceOutside Auto Const

