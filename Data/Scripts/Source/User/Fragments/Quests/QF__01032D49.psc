;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Quests:QF__01032D49 Extends Quest Hidden Const
;BEGIN FRAGMENT Fragment_Stage_0010_Item_00
Function Fragment_Stage_0010_Item_00()
;BEGIN CODE
SetObjectiveDisplayed(10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0020_Item_00
Function Fragment_Stage_0020_Item_00()
;BEGIN CODE
if Game.GetPlayer().GetItemCount(CrayonBox) > 0
	; if player has more than 0 crayon boxes, go right to stage 30
	Debug.Notification("I have 1 or more crayon boxes, going to stage 30")
	SetStage(30)
	SetObjectiveCompleted(20)
	SetObjectiveDisplayed(30, abForce = true)
endif

	SetObjectiveCompleted(10)
	SetObjectiveDisplayed(20, abForce = true)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0030_Item_00
Function Fragment_Stage_0030_Item_00()
;BEGIN CODE
if Game.GetPlayer().GetItemCount(CrayonBox) == 0
	; if player has more than 0 crayon boxes, go back to stage 30
	Debug.Notification("I have less than 1 crayon boxes, going back to stage 20")
	SetStage(20)
	SetObjectiveDisplayed(20, abForce = true)
else
	Debug.Notification("I have at least 1 crayon boxes, going back to stage 20")
	SetObjectiveCompleted(20)
	SetObjectiveDisplayed(30, abForce = true)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0040_Item_00
Function Fragment_Stage_0040_Item_00()
;BEGIN CODE
Game.GetPlayer().RemoveItem(CrayonBox, 1)
Game.GetPlayer().AddItem(pCapsRewardSmall, 1)
Alias_LBT_Alias_CrazyMarty.GetActorRef().AddItem(CrayonBox, 1)
FollowersScript.SendAffinityEvent(self, Game.GetCommonProperties().CA__Event_DonateItem)
SetObjectiveCompleted(30)
SetObjectiveDisplayed(40, abForce = true)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0060_Item_00
Function Fragment_Stage_0060_Item_00()
;BEGIN CODE
CompleteAllObjectives()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0100_Item_00
Function Fragment_Stage_0100_Item_00()
;BEGIN CODE
FailAllObjectives()
Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Potion Property CrayonBox Auto Const

ReferenceAlias Property Alias_LBT_Alias_CrazyMarty Auto Const Mandatory

LeveledItem Property pCapsRewardSmall Auto Const

GlobalVariable Property XPRadiant Auto Const
