;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Scenes:SF_LBT_QST_CrazyMartyDialogu_01001523 Extends Scene Hidden Const

;BEGIN FRAGMENT Fragment_Phase_01_End
Function Fragment_Phase_01_End()
;BEGIN CODE
WorkshopParent.AddPermanentActorToWorkshopPlayerChoice(CrazyMartyObjectRef as Actor)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ReferenceAlias Property CrazyMartyRef Auto Const
{Crazy Marty}

ObjectReference Property CrazyMartyObjectRef Auto Const
{Object ref in marty's cell}

workshopparentscript Property WorkshopParent Auto Const
