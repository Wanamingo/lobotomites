;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_LBT_Terminal_BRAIN_MQ_0101415A Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
;Player now read the important bits of the terminal and needs to go find the holotape
LBT_QUST_MQ.SetStage(33)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property LBT_QUST_MQ Auto Const
{LBT_QUST_MQ, the main quest}
