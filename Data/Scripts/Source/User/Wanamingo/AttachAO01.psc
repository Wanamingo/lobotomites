Scriptname Wanamingo:AttachAO01 extends activemagiceffect


VisualEffect Property MahBladeFX00 Auto
VisualEffect Property MahBladeFX01 Auto
VisualEffect Property MahBladeFX02 Auto
;String Property TurningOnThing00 Auto Const
;String Property TurningOffThing00 Auto Const
;String Property Closed00 Auto Const
;String Property Open00 Auto Const
;Keyword Property LBT_dn_ProtonAxe_ShortGrip Auto Const
;float property ImWaiting = 1.5 auto
;import utility


Event OnEffectStart(Actor akTarget, Actor akCaster)
	debug.trace("DoingSomething")

	MahBladeFX00.Play(akCaster)
	MahBladeFX01.Play(akCaster)
	MahBladeFX02.Play(akCaster)

		debug.trace("FinishedDoingSomething")

EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)

	MahBladeFX00.Stop(akCaster)
	MahBladeFX01.Stop(akCaster)
	MahBladeFX02.Stop(akCaster)

EndEvent


Event OnDeath(Actor akKiller)
	actor selfRef = self.GetTargetActor()
	MahBladeFX00.Stop(selfRef)
	MahBladeFX01.Stop(selfRef)
	MahBladeFX02.Stop(selfRef)

EndEvent



;/
Event OnEffectStart(Actor akTarget, Actor akCaster)
	debug.trace("DoingSomething")

	MahBladeFX00.Play(akCaster)
	MahBladeFX01.Play(akCaster)
	MahBladeFX02.Play(akCaster)
;	if (akCaster.Wornhaskeyword(LBT_dn_ProtonAxe_ShortGrip))
;		Utility.wait(5.0)
;		debug.notification("WeWatied")
;	Else
;		debug.notification("WeDidn'tWait")
;	EndIF
;		Utility.wait(1.5)
;	akCaster.playgamebryoanimation("TurningOnThing00")
		debug.trace("FinishedDoingSomething")

EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
;	akCaster.playgamebryoanimation(TurningOffThing00)
;		Utility.wait(ImWaiting)
	MahBladeFX00.Stop(akCaster)
	MahBladeFX01.Stop(akCaster)
	MahBladeFX02.Stop(akCaster)

EndEvent


Event OnDeath(Actor akKiller)
	actor selfRef = self.GetTargetActor()
	MahBladeFX00.Stop(selfRef)
	MahBladeFX01.Stop(selfRef)
	MahBladeFX02.Stop(selfRef)
;	selfRef.playgamebryoanimation(Closed00)
EndEvent
/;