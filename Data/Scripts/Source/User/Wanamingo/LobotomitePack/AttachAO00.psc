Scriptname Wanamingo:LobotomitePack:AttachAO00 extends activemagiceffect

Actor Property PlayerREF Auto

VisualEffect Property MahHeadThing00 Auto
VisualEffect Property MahHeadThing01 Auto

import utility

Int MahRandomNumber00
actor selfRef

Event OnEffectStart(Actor akTarget, Actor akCaster)
	if  (akCaster as objectReference).WaitFor3DLoad()
		selfRef = self.GetTargetActor()
		debug.Trace("LoboLALALA")
		MahRandomNumber00 = Utility.RandomInt(0, 10)
		If akCaster == PlayerREF
			If MahRandomNumber00 <= 6
				MahHeadThing01.Play(akCaster)
			else
				MahHeadThing00.Play(akCaster)
			Endif
		else
			If MahRandomNumber00 <= 2
				MahHeadThing01.Play(akCaster)
			else
				MahHeadThing00.Play(akCaster)
			Endif
		Endif
	Endif
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	debug.Trace("LoboNONONO")
	MahHeadThing00.Stop(akCaster)
	MahHeadThing01.Stop(akCaster)
EndEvent


Event OnDying(Actor akKiller)
	If self.GetTargetActor() == PlayerREF
		MahHeadThing00.Stop(selfRef)
		MahHeadThing01.Stop(selfRef)
	else
		;Do Nothing.

	Endif
EndEvent

Event OnDeath(Actor akKiller)
	If self.GetTargetActor() == PlayerREF
		;Do Nothing.
	else
		If MahRandomNumber00 <= 2
			MahHeadThing01.Stop(selfRef)
			utility.wait(0.3)
			MahHeadThing01.Play(selfRef)
		else
			MahHeadThing00.Stop(selfRef)
			utility.wait(0.3)
			MahHeadThing00.Play(selfRef)
		Endif
	Endif
	debug.Trace("LoboDead")
EndEvent

