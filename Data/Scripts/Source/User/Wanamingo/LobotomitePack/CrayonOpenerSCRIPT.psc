ScriptName Wanamingo:LobotomitePack:CrayonOpenerSCRIPT extends ActiveMagicEffect Const
{ This script causes stickers to be added to the inventory via LLs to anyone that opens a packet. Script made by Wanaming0 }


;-- Properties --------------------------------------
LeveledItem Property Crayons Auto Const
; LL with all the crayons

;-- Variables ---------------------------------------

;-- Functions ---------------------------------------

Event OnEffectStart(Actor akTarget, Actor akCaster)

    akCaster.additem(Crayons, 1, True)

EndEvent

;import nif lmao