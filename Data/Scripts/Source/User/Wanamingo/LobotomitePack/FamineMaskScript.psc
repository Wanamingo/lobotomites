Scriptname Wanamingo:LobotomitePack:FamineMaskScript extends ActiveMagicEffect 

Float Property GlobalAdd Auto Const
GlobalVariable Property ExtraMeatChance Auto Const


Event OnEffectStart(Actor akTarget, Actor akCaster)
		ExtraMeatChance.SetValue(ExtraMeatChance.GetValue() + GlobalAdd)
EndEvent
