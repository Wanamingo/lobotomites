Scriptname Wanamingo:LobotomitePack:FamineRegenScript extends Actor


String Property animEventName = "weaponSwing" Auto Const
{anim event that calls the spell. WeaponSwing is the one when NPC does a melee attack!}
float property minHealthPercent Auto Const
{Minimum health until the regen ability kicks in}
Spell Property LBT_SPEL_FatMinDrinkBloodSpell Auto Const
{Fat Min's Regeneration Spell}
ActorValue property HealthAV Auto
{Just use Health for this! EnduranceCondition only makes so if they are crippled it kicks in}
float myHealth


Event OnLoad()
	if Is3DLoaded()
	    RegisterForAnimationEvent(self, animEventName)
	    RegisterForHitEvent(self)
	endIf
EndEvent

Event OnUnload()
	UnregisterForAllHitEvents()
EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (akSource == self) && (asEventName == animEventName)
		Debug.Trace("anim event caught")		
		regenerate()
	endIf
EndEvent

Event OnHit(ObjectReference akTarget, ObjectReference akAggressor, Form akSource, Projectile akProjectile, bool abPowerAttack, bool abSneakAttack, bool abBashAttack, bool abHitBlocked, string asMaterialName)
	debug.trace("I got hit")
	Debug.Trace(akTarget + " was hit by " + akAggressor)
	myHealth = self.GetValuePercentage(HealthAV) as float
	debug.trace(myHealth)
	if (myHealth < minHealthPercent)
			UnregisterForAllHitEvents()
			Debug.Trace("Famine has low health!")
			regenerate()
	else
		RegisterForHitEvent(self)
	endIf
EndEvent

Event OnDeath(Actor akKiller)
	UnregisterForAllHitEvents()
EndEvent

function regenerate()
	debug.trace("Famine cures himself!")
endFunction