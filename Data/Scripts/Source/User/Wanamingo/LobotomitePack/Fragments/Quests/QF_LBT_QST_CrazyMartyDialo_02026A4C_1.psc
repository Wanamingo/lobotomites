;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Wanamingo:LobotomitePack:Fragments:Quests:QF_LBT_QST_CrazyMartyDialo_02026A4C_1 Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0010_Item_00
Function Fragment_Stage_0010_Item_00()
;BEGIN CODE
LBT_QUST_CrazyMartyQuest.SetStage(10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0020_Item_00
Function Fragment_Stage_0020_Item_00()
;BEGIN CODE
if Game.GetPlayer().GetItemCount(CrayonBox) > 0
	; if player has more than 0 crayon boxes, go right to stage 30
	SetStage(30)
                LBT_QUST_CrazyMartyQuest.SetStage(30)  
endif
               LBT_QUST_CrazyMartyQuest.SetStage(20)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0030_Item_00
Function Fragment_Stage_0030_Item_00()
;BEGIN CODE
if Game.GetPlayer().GetItemCount(CrayonBox) == 0
	; if player has more than 0 crayon boxes, go back to stage 30
	SetStage(20)
                LBT_QUST_CrazyMartyQuest.SetStage(20)
else
	Debug.Notification("I have at least 1 crayon boxes, going back to stage 20")
                LBT_QUST_CrazyMartyQuest.SetStage(30)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0031_Item_00
Function Fragment_Stage_0031_Item_00()
;BEGIN CODE
LBT_QUST_CrazyMartyQuest.SetStage(31)
SetStage(31)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0040_Item_00
Function Fragment_Stage_0040_Item_00()
;BEGIN CODE
LBT_QUST_CrazyMartyQuest.SetStage(40)
SetStage(40)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0100_Item_00
Function Fragment_Stage_0100_Item_00()
;BEGIN CODE
LBT_QUST_CrazyMartyQuest.SetStage(100)
Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Potion Property CrayonBox Auto Const

ReferenceAlias Property Alias_LBT_Alias_CrazyMarty Auto Const Mandatory

LeveledItem Property pCapsRewardSmall Auto Const

GlobalVariable Property XPRadiant Auto Const

Quest Property LBT_QUST_CrazyMartyQuest Auto Const
{The actual quest that completes when you give him crayons}
