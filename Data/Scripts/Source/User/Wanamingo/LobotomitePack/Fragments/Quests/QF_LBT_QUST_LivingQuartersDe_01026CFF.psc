;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Wanamingo:LobotomitePack:Fragments:Quests:QF_LBT_QUST_LivingQuartersDe_01026CFF Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0100_Item_00
Function Fragment_Stage_0100_Item_00()
;BEGIN CODE
Debug.Trace("Living Quarters Deadly Gas Off")
Alias_LBT_Alias_DeadlyGas_ParentEnable.GetRef().Disable()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ReferenceAlias Property LBT_Alias_LivingQuartersDeadlyGas Auto Const

RefCollectionAlias Property LBT_Alias_LivingQuartersDeadlyGasRefColl Auto Const

ReferenceAlias Property Alias_LBT_Alias_DeadlyGas_ParentEnable Auto Const Mandatory
