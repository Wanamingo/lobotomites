;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Wanamingo:LobotomitePack:Fragments:Terminals:TERM_LBT_TERM_DisableBRAINno_0101E4A5 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
;Player now read the important bits of the holotape
LBT_QUST_MQ.SetStage(34)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property LBT_QUST_MQ Auto Const
