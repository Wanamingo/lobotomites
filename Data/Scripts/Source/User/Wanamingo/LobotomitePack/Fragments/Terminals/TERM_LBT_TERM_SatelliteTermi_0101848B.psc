;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Wanamingo:LobotomitePack:Fragments:Terminals:TERM_LBT_TERM_SatelliteTermi_0101848B Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
pLBT_QUST_MQ.SetStage(80)
pLBT_QUST_MQ.SetStage(84)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
pLBT_QUST_MQ.SetStage(80)
pLBT_QUST_MQ.SetStage(86)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
pLBT_QUST_MQ.SetStage(80)
pLBT_QUST_MQ.SetStage(82)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Quest Property pLBT_QUST_MQ Auto Const
