Scriptname Wanamingo:LobotomitePack:HeartDeath00 extends Actor

VisualEffect Property LBT_VE_BlahHeart00 Auto ;Female
VisualEffect Property LBT_VE_BlahHeart01 Auto ;Male

VisualEffect Property LBT_VE_BlahHeartSparks00 Auto
VisualEffect Property LBT_VE_BlahHeartSparks01 Auto
Keyword Property LBT_KW_BlahHeart00 Auto Const
Keyword Property LBT_KW_BlahHeart01 Auto Const

MiscObject Property LBT_Cyberpart_Spine auto
MiscObject Property LBT_Cyberpart_Heart auto
MiscObject Property LBT_Cyberpart_BrainCoil auto
MovableStatic Property LBT_MSTT_BlahTorsoDebris00 auto
ObjectReference MahInsides00

ActorValue Property pEnduranceCondition Auto
Float Property EffectsTimer Auto Const
Actor Property PlayerREF Auto

Spell Property LBT_Spell_BlahDish00 Auto

Sound Property LBT_DRS_BlahHeart00 Auto
Int Property HeartBeatSoundInt00 auto
Float Property HeartBeatSoundVol00 auto

bool Property OnlyOnce00 auto
bool Property OnlyOnce01 auto
bool Property OnlyOnce02 auto
bool Property OnlyOnce03 auto
bool Property OnlyOnce04 auto
import utility

bool AlreadyDone00
bool AlreadyDead00


;If self.IsDismembered(string asBodyPart = "Torso")
;		Sound.StopInstance(BoatEngineSoundRad01)

Event OnLoad()
	while !(self.Is3DLoaded())
		utility.wait(0.3)
	endwhile
	RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
	If !(self.IsDismembered("Torso"))
		If OnlyOnce00 == false 
			self.AddSpell(LBT_Spell_BlahDish00)
			AddInventoryEventFilter(LBT_Cyberpart_Spine)
			AddInventoryEventFilter(LBT_Cyberpart_Heart)
			AddInventoryEventFilter(LBT_Cyberpart_BrainCoil)
			Debug.trace("Got the stuff!!!!!")
			OnlyOnce00 = true
		endif
		utility.wait(0.5)
		while !(self.Is3DLoaded())
			utility.wait(0.3)
		endwhile
		If ((OnlyOnce02 == true) || (OnlyOnce01 == true))
			LBT_VE_BlahHeart00.Stop(self)
			LBT_VE_BlahHeart01.Stop(self)
			utility.wait(0.3)
			HeartBeatSoundInt00 = LBT_DRS_BlahHeart00.Play(Self)
			Sound.SetInstanceVolume(HeartBeatSoundInt00, HeartBeatSoundVol00)
			if self.GetLeveledActorBase().GetSex() == 1 ;Female
				LBT_VE_BlahHeart00.Play(self)
			Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
				LBT_VE_BlahHeart01.Play(self)
			endif
		endif
		LBT_VE_BlahHeartSparks00.Stop(self)
		LBT_VE_BlahHeartSparks01.Stop(self)
		if !(self.isdead())
			utility.wait(0.3)
			If (self.wornhaskeyword(LBT_KW_BlahHeart00))
				LBT_VE_BlahHeart00.Stop(self)
				LBT_VE_BlahHeart01.Stop(self)
			ElseIf !(self.wornhaskeyword(LBT_KW_BlahHeart00))
				If ((OnlyOnce02 == false) && (OnlyOnce01 == false))
					utility.wait(0.3)
					if self.GetLeveledActorBase().GetSex() == 1 ;Female
						LBT_VE_BlahHeart00.Play(self)
					Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
						LBT_VE_BlahHeart01.Play(self)
					endif
					Debug.trace("Show Heart!!!!!")
					OnlyOnce02 = true
				endif
			endif
		endif
	endif

EndEvent
;/
Event OnContainerChanged(ObjectReference akNewContainer, ObjectReference akOldContainer)
	if (Game.GetPlayer().GetItemCount(WeapTypeSwordKeyword) == 0)

	endif
EndEvent
/;

Event Actor.OnPlayerLoadGame(Actor akSender)
	while !(self.Is3DLoaded())
		utility.wait(0.3)
	endwhile
	RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
	utility.wait(0.3)
	LBT_VE_BlahHeartSparks00.Stop(self)
	LBT_VE_BlahHeartSparks01.Stop(self)
	LBT_VE_BlahHeart00.Stop(self)
	LBT_VE_BlahHeart01.Stop(self)
	utility.wait(0.3)
	If !(self.IsDismembered("Torso"))
		If !(self.wornhaskeyword(LBT_KW_BlahHeart00))
			if self.GetLeveledActorBase().GetSex() == 1 ;Female
				LBT_VE_BlahHeart00.Play(self)
			Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
				LBT_VE_BlahHeart01.Play(self)
			endif
		elseIf (self.wornhaskeyword(LBT_KW_BlahHeart00))
			utility.wait(0.3)
			LBT_VE_BlahHeart00.Stop(self)
			LBT_VE_BlahHeart01.Stop(self)
			LBT_VE_BlahHeart00.Stop(self)
			LBT_VE_BlahHeart01.Stop(self)
		endif
	endif
	Sound.StopInstance(HeartBeatSoundInt00)
	Debug.trace("PlayerLoad!!!!!")
EndEvent

Event OnItemRemoved(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	debug.Trace("Dismember0")
	if (akBaseItem == LBT_Cyberpart_Spine) || (akBaseItem == LBT_Cyberpart_Heart)
		debug.Trace("SpineHeart1")
		If OnlyOnce03 == false 
			debug.Trace("SpineHeart2")
			self.Dismember("Head1", true, true, true)
			self.Dismember("Head2", true, true, true)
			MahInsides00 = self.placeatme(LBT_MSTT_BlahTorsoDebris00)
			self.Dismember("Torso", true, true, true)
			LBT_VE_BlahHeart00.Stop(self)
			LBT_VE_BlahHeart01.Stop(self)
			LBT_VE_BlahHeartSparks00.Stop(self)
			LBT_VE_BlahHeartSparks01.Stop(self)
			Sound.StopInstance(HeartBeatSoundInt00)
			utility.wait(0.2)
			MahInsides00.DamageObject(10.0)
			if !(self.isdead())
				self.Kill()
			endif
			OnlyOnce03 = true
		endif
	endIf
	if (akBaseItem == LBT_Cyberpart_BrainCoil)
		debug.Trace("Brain1")
		If OnlyOnce03 == false 
			If OnlyOnce04 == false 
				debug.Trace("Brain2")
				self.Dismember("Head1", true, true, true)
				self.Dismember("Head2", true, true, true)
				if !(self.isdead())
					self.Kill()
				endif
				OnlyOnce04 = true
			endif
		endIf
	endIf
			debug.Trace("Dismember3")
endEvent

Event OnUnload()
			MahInsides00 = none

EndEvent

Event OnItemUnequipped(Form akBaseObject, ObjectReference akReference)
	If !(self.IsDismembered("Torso"))
		if akBaseObject as Armor
			debug.Trace("Armor!!!!!")
			utility.wait(0.1)
			If !(self.wornhaskeyword(LBT_KW_BlahHeart00))
				If ((OnlyOnce01 == false) && (OnlyOnce02 == false))
					if self.GetLeveledActorBase().GetSex() == 1 ;Female
						LBT_VE_BlahHeart00.Play(self)
					Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
						LBT_VE_BlahHeart01.Play(self)
					endif
					debug.Trace("ErrMahGerd!!!!!")
					OnlyOnce01 = true
				endif
			else
				debug.Trace("Whatev's")
			endif
		endif
	endif
EndEvent

Event OnCripple(ActorValue akActorValue, bool abCrippled)
	debug.Trace("LoboCrippleHeartGo")
	if akActorValue == pEnduranceCondition
		utility.wait(0.3)
		if self.GetLeveledActorBase().GetSex() == 1 ;Female
			LBT_VE_BlahHeartSparks00.Play(self, 10.0)
		Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
			LBT_VE_BlahHeartSparks01.Play(self, 10.0)
		endif
		debug.Trace("LoboCrippleHeartDone")
	endIf
EndEvent

Event OnDying(Actor akKiller)
	if self.GetLeveledActorBase().GetSex() == 1 ;Female
		LBT_VE_BlahHeartSparks00.Play(self, 10.0)
	Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
		LBT_VE_BlahHeartSparks01.Play(self, 10.0)
	endif

EndEvent

Event OnDeath(Actor akKiller)
	if AlreadyDead00 == False
		StartTimer(EffectsTimer, 0)
		if self.GetLeveledActorBase().GetSex() == 1 ;Female
			LBT_VE_BlahHeartSparks00.Play(self, EffectsTimer)
		Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
			LBT_VE_BlahHeartSparks01.Play(self, EffectsTimer)
		endif
		debug.Trace("LoboHeartDead")
		AlreadyDead00 = True
	endif
EndEvent

Event OnKill(Actor akVictim)
	if AlreadyDead00 == False
;/
		if self.GetLeveledActorBase().GetSex() == 1 ;Female
			LBT_VE_BlahHeartSparks00.Play(self, 10.0)
		Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
			LBT_VE_BlahHeartSparks01.Play(self, 10.0)
		endif
/;
		if (akVictim == self)
			Debug.notification("I haz been killded!")
			StartTimer(EffectsTimer, 0)
			if self.GetLeveledActorBase().GetSex() == 1 ;Female
				LBT_VE_BlahHeartSparks00.Play(self, EffectsTimer)
			Elseif self.GetLeveledActorBase().GetSex() == 0 ;Male
				LBT_VE_BlahHeartSparks01.Play(self, EffectsTimer)
			endif
			debug.Trace("LoboHeartDead")
		endIf
		AlreadyDead00 = True
	endIf
endEvent
;/
Event OnKill(Actor akVictim)
	if (akVictim == self)
		Debug.Trace("I haz been killded!")
		LBT_VE_BlahHeart00.Stop(self)
		LBT_VE_BlahHeart01.Stop(self)
		LBT_VE_BlahHeartSparks00.Stop(self)
		LBT_VE_BlahHeartSparks01.Stop(self)
	endIf
endEvent
/;


Event OnTimer(int aiTimerID)	
	if (aiTimerID == 0)
		LBT_VE_BlahHeartSparks00.stop(self)
		LBT_VE_BlahHeartSparks01.stop(self)
		Sound.StopInstance(HeartBeatSoundInt00)
	endif
EndEvent