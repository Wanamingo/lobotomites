ScriptName Wanamingo:LobotomitePack:Integration extends Quest

String LAER_PLUGIN_NAME = "LAER.esp"
String WJSSLEDGE_PLUGIN_NAME = "ClassicSuperSledge.esl"
String AUTOMATRON_PLUGIN_NAME = "DLCRobot.esm"
String NUKAWORLD_PLUGIN_NAME = "DLCNukaWorld.esm"
String WASTELANDWORKSHOP_PLUGIN_NAME = "DLCWorkshop01.esm"
String CONTRAPTIONS_PLUGIN_NAME = "DLCWorkshop02.esm"

bool LAERInjected
bool ClassicSledgeInjected
bool AutomatronInjected
bool NWRegistered

;--Properties--

Group LobotomiteLLs
  LeveledItem Property LBT_LobotomiteLL_Rifle_Semiauto_LAER Auto Const
  LeveledItem Property LBT_LobotomiteLL_SemiautoPistol_LAER Auto Const
  LeveledItem Property LBT_LobotomiteLL_GenericRaiderRangedWeapons Auto Const
EndGroup

Group Sledges
  LeveledItem Property LBT_ClassicSaturniteSledge Auto
  LeveledItem Property LBT_VanillaSaturniteSledge Auto
  LeveledItem Property LBT_WarWeaponLL Auto
EndGroup


Group Globals
  GlobalVariable Property LBT_HasCobalt Auto
  GlobalVariable Property LBT_WWInstalled Auto
  GlobalVariable Property LBT_ContraptionsInstalled Auto
EndGroup

;--Integers--

int FeaturedItem_ID = 0x1b3fac
int DLC04PrjectCobaltSchematics_ID = 0x056cf9 ; ID of the schematics
int DLC04HasProjectCobalt_ID = 0x053884 ; ID of cobalt global

Group Integers
  int Property PlayerLevel Auto Const Mandatory
EndGroup

Keyword FeaturedItem
Form DLC04PrjectCobaltSchematics
GlobalVariable DLC04HasProjectCobalt

Event OnQuestInit()
  RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
  Handlers()
EndEvent

Event OnQuestShutdown()
EndEvent

Event Actor.OnPlayerLoadGame(Actor ActorRef)
  Handlers()
EndEvent

Event ObjectReference.OnItemAdded(ObjectReference akSender, Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
  If (akBaseItem == DLC04PrjectCobaltSchematics)  
    LBT_HasCobalt.SetValueInt(1)
    UnregisterForRemoteEvent(Game.GetPlayer(), "OnItemAdded")
  EndIf
EndEvent

Function Handlers()
  If (Game.IsPluginInstalled(WASTELANDWORKSHOP_PLUGIN_NAME))
    LBT_WWInstalled.SetValueInt(1)
  Else
    LBT_WWInstalled.SetValueInt(0)
  EndIf
  
  If (Game.IsPluginInstalled(CONTRAPTIONS_PLUGIN_NAME))
    LBT_ContraptionsInstalled.SetValueInt(1)
  Else
    LBT_ContraptionsInstalled.SetValueInt(0)
  EndIf

  LAERHandler()
  ClassicSledgeHandler()
  AutomatronHandler()
  NukaWorldHandler()
EndFunction

Function LAERHandler()
  If (Game.IsPluginInstalled(LAER_PLUGIN_NAME))
    If (LAERInjected)
	  Return
    EndIf
	Debug.Notification("LobotomitePack: LAER mod detected. Injecting into Lobotomite LLs")
    Debug.Trace("LobotomitePack: LAER mod detected. Injecting into Lobotomite LLs")
; LL fuckery goes here
    LeveledItem LL_LAER_Worn_Rifle_SemiAuto = Game.GetFormFromFile(0x1F04, "LAER.esp") as LeveledItem
    LeveledItem LL_LAER_Worn_SemiautoPistol = Game.GetFormFromFile(0x8250, "LAER.esp") as LeveledItem
    LBT_LobotomiteLL_Rifle_Semiauto_LAER.AddForm(LL_LAER_Worn_Rifle_SemiAuto, PlayerLevel, 1)
    LBT_LobotomiteLL_SemiautoPistol_LAER.AddForm(LL_LAER_Worn_SemiautoPistol, PlayerLevel, 1)
    LAERInjected = True
  Else
    Debug.Trace("LobotomitePack: LAER mod NOT detected. Using ranged vanilla weapons instead.")
	LBT_LobotomiteLL_Rifle_Semiauto_LAER.AddForm(LBT_LobotomiteLL_GenericRaiderRangedWeapons, PlayerLevel, 1)
	LBT_LobotomiteLL_SemiautoPistol_LAER.AddForm(LBT_LobotomiteLL_GenericRaiderRangedWeapons, PlayerLevel, 1)
    LAERInjected = False
  EndIf
EndFunction

Function ClassicSledgeHandler()
  If (Game.IsPluginInstalled(WJSSLEDGE_PLUGIN_NAME))
    If (ClassicSledgeInjected)
      Return
    EndIf
    Debug.Trace("LobotomitePack: Classic Super Sledge mod detected. Injecting Classic Saturnite Sledge into War's LL")
; LL fuckery goes here
    LBT_WarWeaponLL.AddForm(LBT_ClassicSaturniteSledge, PlayerLevel, 1)
    ClassicSledgeInjected = True
  Else
    Debug.Trace("LobotomitePack: Classic Super Sledge mod NOT detected. Injecting vanilla Saturnite Sledge into War's LL")
    LBT_WarWeaponLL.AddForm(LBT_VanillaSaturniteSledge, PlayerLevel, 1)
    ClassicSledgeInjected = False
  EndIf
EndFunction

Function AutomatronHandler()
  If (Game.IsPluginInstalled(AUTOMATRON_PLUGIN_NAME))
    If (AutomatronInjected)
      Return
    EndIf
; formlist/keyword stuff goes here (if possible without hard dependency)
    Debug.Trace("LobotomitePack: Automatron detected.")
    AutomatronInjected = True
  Else
    Debug.Trace("LobotomitePack: Automatron NOT detected.")
    AutomatronInjected = False
  EndIf
EndFunction

Function NukaWorldHandler()  
  If (Game.IsPluginInstalled(NUKAWORLD_PLUGIN_NAME) && LBT_HasCobalt.GetValueInt() == 0)
    DLC04HasProjectCobalt = Game.GetFormFromFile(DLC04HasProjectCobalt_ID, NUKAWORLD_PLUGIN_NAME) as GlobalVariable
    If (DLC04HasProjectCobalt.GetValueInt() == 1)
      LBT_HasCobalt.SEtValueInt(1)
      Return
    EndIf
    If (NWRegistered)
      Return
    EndIf
    Debug.Trace("LobotomitePack: Nuka world detected.")
	
    DLC04PrjectCobaltSchematics = Game.GetFormFromFile(DLC04PrjectCobaltSchematics_ID, NUKAWORLD_PLUGIN_NAME)
    FeaturedItem = Game.GetFormFromFile(FeaturedItem_ID, "Fallout4.esm") as Keyword
	
    RegisterForRemoteEvent(Game.GetPlayer(), "OnItemAdded")
    AddInventoryEventFilter(FeaturedItem)
  ElseIf (NWRegistered)
    UnregisterForRemoteEvent(Game.GetPlayer(), "OnItemAdded")
    NWRegistered = False
  EndIf
EndFunction