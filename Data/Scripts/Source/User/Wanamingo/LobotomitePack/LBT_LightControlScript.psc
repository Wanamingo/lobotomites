Scriptname Wanamingo:LobotomitePack:LBT_LightControlScript extends ObjectReference 
{Controls the light's color depending on player's action}

Int Property lightColor Auto 
String[] Property anim Auto Const

; 0 = off
; 1 = green
; 2 = yellow 
; 3 = red

event OnCellAttach()
	SetLight()
endEvent

Function SetLight()
	PlayGamebryoAnimation(anim[lightColor])
	Debug.Trace("Light is trying to play anim: "+anim[lightColor])
endFunction