Scriptname Wanamingo:LobotomitePack:MQKeypadScript extends ObjectReference

Quest Property pLBT_QUST_MQ Auto Const

Quest Property pLBT_QUST_MQKeypad Auto Const

Scene Property pIncorrectCode Auto Const

Sound Property ActivateSound Auto Const

int Property StageDoneNumber1KeypadN Auto Const

int Property StageDoneNumber2KeypadK Auto Const

int Property StageDoneNumber3KeypadActivated Auto Const

bool bCanPlayAgain = true


Event OnActivate(ObjectReference akActionRef)
;When activating this object, if the proper stage has been reached, and the player's not in combat, play scene
;Otherwise, player negative noise if quest has started and "Incorrect Code" scene if it hasn't

	Debug.Notification("Anything activated me")

	if !pLBT_QUST_MQ.GetStageDone(StageDoneNumber1KeypadN) && !pLBT_QUST_MQ.GetStageDone(StageDoneNumber2KeypadK)
		Debug.Notification("First condition is OKAY")
		if akActionRef == Game.GetPlayer()
			Debug.Notification("Yes I'm the player, dumbass") 
			if pLBT_QUST_MQ.GetStageDone(StageDoneNumber1KeypadN) && !(Game.GetPlayer()).IsInCombat() && !pLBT_QUST_MQ.GetStageDone(StageDoneNumber2KeypadK)
							Debug.Notification("We want to start the scene here")
				pLBT_QUST_MQ.SetStage(StageDoneNumber3KeypadActivated)
				pLBT_QUST_MQKeypad.SetStage(100)

			else

				if !pIncorrectCode.IsPlaying()

					pIncorrectCode.Start()

				endif

			endif

		endif

	else

		if bCanPlayAgain == true
			bCanPlayAgain = false

			if ActivateSound.PlayAndWait(self)
				bCanPlayAgain = true
			else
				bCanPlayAgain = true
			endif
		endif

	endif

EndEvent
